#!/bin/sh

set -e
tag=$1
version=$2
echo "Building version $version:"

echo "Adding MSG support"
sed -i "s|./configure|apt-get update -y \&\& apt-get install -y unzip \&\& unzip -d gdal/frmts/msg/PublicDecompWT/ PublicDecompWT.zip \&\& ./configure --with-msg|" $version/Dockerfile

docker build -t "$tag" "$version"
